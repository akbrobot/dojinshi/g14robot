
= 格安USBロジアナを sigrok と PulseView で利用する


//raw[|latex| \begin{center} ]
河野悦昌  @<b>{秘密結社オープンフォース}
//raw[|latex| \end{center} ]
Amazon などで購入できる激安ロジアナ。
//indepimage[c0f13aea-a2af-af82-c3e0-3ba57430af05][][scale=0.7]




いろんなところから販売されているが、どれもこれも「Logic Analyzer」ぐらいしか名前がなくて Generic 品風情が強い。しかしその中で KeeYees のロゴが付いているのがあり、おっと思ってこれを選んでみた。



//indepimage[3f266de0-28f2-89f3-d030-1c8ef69c5f32][][scale=0.5]




しばらくして到着したものは・・・
//indepimage[1b9dbfb2-2ba7-6daf-b52d-91ff6b398e0d][][scale=0.5]

結局ノーブランド品ぽいものでした。今回はこれを Ubuntu で動かしてみます。


== 環境
 * Ubuntu 22.04 LTS Desktop Ja


== どんなハードウェアかな?


USBコネクタ。mini-B ですねー。



//indepimage[37db434f-a6ca-5871-a7db-bb75fec8d9d6][][scale=0.5]




中身。
//indepimage[df81a107-e1f9-98ed-86cf-719a7ff7fe00][][scale=0.5]




このロジアナのクロックはUSBのクロックをそのまま使っているというウワサもありあましたが、24MHzのクリスタルが乗っていました。



付属のジャンパーケーブルをコネクタに刺します。カラーコードを合わせてみましたが、後で使う sigrok のピン番号とずれているのでちょっと問題。



//indepimage[40844e83-a509-2cfc-49e0-7fd199b525cd][][scale=0.5]




付属のクリップは、自分ではんだづけする必要があります。今回は付属のジャンパーケーブルに刺せるようにするため、ピンを出すことにしました。
ピンヘッダをはんだづけした後に黒いプラスチックを取って組み立てます。
//indepimage[f24afc0f-869c-1349-d694-94f2a98dd852][][scale=0.5]




加工が終わったところ。コネクタの色は6色で2つずつありますが、ケーブルの色の10色には足りないので工夫が必要です。



//indepimage[ad70acb6-472d-bc7b-f649-628a61cc4292][][scale=0.5]



== dmesg の状況


刺してみた。


//emlist{
[128636.623732] usb 3-1.3.3: new high-speed USB device number 11 using xhci_hcd
[128636.749653] usb 3-1.3.3: New USB device found, idVendor=0925, idProduct=3881, bcdDevice= 0.01
[128636.749659] usb 3-1.3.3: New USB device strings: Mfr=0, Product=0, SerialNumber=0
//}


このロジックアナライザは Salae Logic のクローンという話があったので、このVendorIDを調べてみると・・・


//emlist{
$ lsusb
.
.
.
Bus 003 Device 016: ID 0925:3881 Lakeview Research Saleae Logic
.
.
.
//}


なるほどー、真っ黒でした。



Saleae Logic のソフトが使えるという話もあるのですが、オープンソースの sigrok を使うことにします。


== sigrok を動かしてみる


Ubuntu のパッケージからインストール。


//emlist{
$ sudo apt install sigrok
.
.
.
これを削除するには 'sudo apt autoremove' を利用してください。
以下の追加パッケージがインストールされます:
  sigrok-cli sigrok-firmware-fx2lafw
以下のパッケージが新たにインストールされます:
  sigrok sigrok-cli sigrok-firmware-fx2lafw
アップグレード: 0 個、新規インストール: 3 個、削除: 0 個、保留: 247 個。
67.7 kB のアーカイブを取得する必要があります。
この操作後に追加で 327 kB のディスク容量が消費されます。
続行しますか? [Y/n] 
//}


インストールした後、動作確認してみます。


//emlist{
$ sigrok-cli -d fx2lafw --show
Driver functions:
    Logic analyzer
Scan options:
    conn
fx2lafw - Saleae Logic with 8 channels: D0 D1 D2 D3 D4 D5 D6 D7
Channel groups:
    Logic: channels D0 D1 D2 D3 D4 D5 D6 D7
Supported configuration options across all channel groups:
    continuous: on, off
    limit_samples: 0 (current)
    conn: 3.18 (current)
    samplerate - supported samplerates:
      20 kHz (current)
      25 kHz
      50 kHz
      100 kHz
      200 kHz
      250 kHz
      500 kHz
      1 MHz
      2 MHz
      3 MHz
      4 MHz
      6 MHz
      8 MHz
      12 MHz
      16 MHz
      24 MHz
      48 MHz
    Supported triggers: 0 1 r f e 
    captureratio: 0 (current)
//}


動いてますね。ファームウェアも(何故か)インストールされているようです。


== GUI で動かしてみる


PulseView を使って、sigrok を GUI で操作できるようにしてみます。
こちらも Ubuntu のパッケージでインストールします。


//emlist{
$ sudo apt install pulseview
//}


起動したところ
//indepimage[15b8c41c-f629-2732-58e8-0431902a7f66][][scale=0.5]




I2Cの通信を見てみます。BME280 の通信を調べてみます。CH1 に SCL、CH2 に SDA、GNDとGNDをつなぎます。



//indepimage[e8bc45e1-2402-5172-3377-9f83ad9e41b9][][scale=0.5]




PulseView の設定はとりあえずこれにしました。黄色と緑のアイコンを押してデコーダを選択します。
//indepimage[d243d381-36cc-356c-1a45-c54ab67abdc8][][scale=0.5]

デコーダをI2Cにします。
//indepimage[90b62db8-6a6e-8046-f2b9-ab138af09b4b][][scale=0.5]




「Run」を押すとデータが取得できました。D0 が CH1 で SCL, D1 が CH2 で SDA です。
DとCHの番号がズレているのですがどうにかできないのかな?



//indepimage[55ea884a-6f1a-f1d3-0800-6521a35ae2a4]




チャンネル名につけかえ、色も修正して見やすくしました。下のチャンネルに I2C の信号が見えてますね。



//indepimage[6c2be113-703f-de6d-0e85-d65e1f445528]




うーんでもデタラメですね。
サンプリングレートが低すぎたようです。



サンプリングレートを高くして、試してみました。
//indepimage[f700b7c6-9285-c086-3a27-b8f67537f4cc][][scale=0.6]




今度は、BME280 の I2C アドレス 76 がちゃんと見えるようになりました。
//indepimage[695f35be-cd34-3256-a631-30100aac8b69]




これは便利!


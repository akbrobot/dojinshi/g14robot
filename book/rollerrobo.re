
= タミヤ・ローラースケートロボの改造


//raw[|latex| \begin{center} ]
斉藤  @<b>{ロボ部 斉藤}
//raw[|latex| \end{center} ]


== タミヤ・ローラスケートロボをrasPI-3Aで動かしてみた


//indepimage[77a9892d-82fa-ebc1-aa22-e42b06c37bff][][scale=0.85]



== タミヤ・ローラースケートロボとは


両脚を左右に開閉させてすべるように前に進む(スウィズル)、ローラースケートを装備したロボット(@2420)



//indepimage[12fba674-d148-6b72-42f5-be2f800b680e][][scale=1.1]



== 追加部品
 * Raspbery Pi-３A
 * モータ制御モジュール(DRV8830-I2C)
 * 電源：昇圧モジュール(3.7v→5v)
 * ソフトPWM用基板(6ch)（自作）
 * 電池：14500(Lipo 単三電池サイズで 3.7v）＋モバイルバッテリ
 * ジョイスティック(F710)
 * LiDAR-LD06(38.6mmキューブで42g 12mまで)回転が中で安全



//indepimage[fc22ef2b-78da-dd5e-44f6-b61703d5a779][][scale=1.2]



== 取り合えず・・ロボットの組み立て


//indepimage[81ad4c46-d0dc-9367-a798-24340bde24f2][][scale=1]



=== ロボットの組み立て-2


マイクロサーボをブラックの底が平坦のに変更



//indepimage[1476a73b-7912-2660-b72a-9d7380c35973][][scale=1]



=== 左右制御の動作確認


//indepimage[e9df0035-42cc-c94e-476a-21fed06ee54b][][scale=1]




https://twitter.com/i/status/1599038555661430784


=== rasPI装着と動作確認でrasPIが飛ぶ


//indepimage[d974878b-db28-6efa-f195-cdb4f02e4f24][][scale=1]



== 動かしてみた(ジョイスティック操作)


//indepimage[3beadfa7-ec6e-0ea4-23e5-f56f1aedda02][][scale=1.2]




https://twitter.com/i/status/1599037939216154625



https://twitter.com/i/status/1599038110167609345


=== LiDAR LD06を付けてみた


//indepimage[2c40dcc5-6545-5bab-cdeb-9fd08f831c0c][][scale=1]



=== LiDAR LD06をROS-rvizで確認


//indepimage[9f0180e4-138d-b86b-df50-ad98a705f1c1][][scale=1]



== 最後に・・


ローラスケートロボでは、ブレーキが無い!

 * 可動モータを停止しても、慣性で動いてしまいます。
 * そこで・・スキーのボーゲンを参考に「プルークファーレン」で停止する方法で・・左右のサーボを広げて止める制御を入れた。



やりたい事

 * WEB操作を追加してみんなで外部から動かせたい。
 * (前々回ngrok経由でロボット動かしたように)
 * ROSで自律走行まで
 * カメラも取り付けたい
 * スピーカマイクも付けてお喋りも。。



= 秋葉原自動農園（日記）


//raw[|latex| \begin{center} ]
村田  @<b>{秋葉原ロボット部}
//raw[|latex| \end{center} ]
スマートホームの延長線上でベランダ菜園を自動化しました。



ここは秋葉原は外神田３丁目ジャンク通りの裏通り築45年の文化マンションの5階です。窓からは神田明神を望む西向きのベランダの自動農園です。


== ■材料
 * タカギ(takagi) 全自動洗濯機用分岐栓 B490 洗濯機 分岐 分水 【安心の2年間保証】→洗濯機からの水の分水
 * DC12V 電磁弁 圧縮水空気用 のための接続用
 * カクダイ ネジ口金 13 568-010
 * ジャンク通りので買った12Vアダプター→水のオンオフ用電磁弁、電磁弁とジョイント
 * タカギ(takagi) ホース クリア耐圧ホース15×20 003M 3m 耐圧 透明 PH08015CB003TM
 * タカギ(takagi) ホース ジョイント コネクター 普通ホース G079FJ 【安心の2年間保証】→分水用ホースとジョイント、水回りはタカギが安牌
 * Smilerain 30M 自動散水セット 自動水やり 灌漑システム 点滴ノズル 噴霧ノズル ドリップ散水 給水システム 散水用具 散水灌漑 散水ホース ガーデン 花 植物 盆栽用 家庭園芸 植物育成 温室加湿→灌漑システムメイン電磁弁以降を繋いで各植木鉢に散水ノズルを埋め込む洗濯機用の吸水を分岐して電磁弁を装着し電磁弁の開閉をスマートプラグで制御する。Raspberry Piなどを使って制御すればやれることは増えそうだが今回は水やりのみ、水さえ大丈夫なら植物は元気に育ちます。
 * スイッチボットの500円カメラ→監視用



//indepimage[3fa98a5c-cb5c-565a-5144-4378b11d0ab5][][scale=0.3]

//indepimage[33d8b9e9-60cd-a633-d41e-1e3f93599228][][scale=0.5]



== ■栽培した植物
 * またたび（飼い猫用）
 * ゴーヤ（千代田区ベランダ緑化レポート用）
 * ツルムラサキ（千代田区ベランダ緑化レポート用）
 * びわ（食用）
 * キャロライナ・リーパー（罰ゲーム用）
 * チョウセンアサガオ（トリップ用）


== ■観察日記


6/18　右端にゴーヤの苗（左端は猫用のまたたび、真ん中はキャロライナリーパーです。）
//indepimage[fbf7a49e-7b9e-3c2f-dc15-ff50d941d77a][][scale=0.5]




6/25　右下に伸び始めたゴーヤ、ツルムラサキの葉っぱはテカテカして力強い。
（画面右上ラップにくるまれた装置がタイマー手動対応wifi水やり器）
//indepimage[cf853e18-e797-1332-8885-978073784f2a][][scale=0.3]




7/10　ネットにゴーヤのつるが絡みついていい感じ、観察していたがかなりつるが細かったのが印象に残った、ツルムラサキのつるが相対的にめちゃくちゃ太かったからかもしれない。
//indepimage[845ce314-2fcb-7338-bbb3-3aa1b6d4d12b][][scale=0.3]




8/4　だいぶ上まで伸びてきた、やはりゴーヤのつるは細い、下の方の葉っぱが枯れてきてるこの範囲だけ枯れているのは近くに植えてるチョウセンアサガオの影響だろうか・・・？ツルムラサキはごん太という感じである。
//indepimage[a272420c-0e70-625d-1c3f-e6fb335c87d6][][scale=0.55]




9/12　ゴーヤの下の部分1m位は枯れているがなぜかそれ以降は元気のようだ、2mほど上の物干し竿から折り返して1.5ｍほど垂れ下がっている。ツルムラサキもごん太のつるが上から折り返して地面を這うくらいまで来てる。写真２枚めはたわわに実ったチョウセンアサガオの実
//indepimage[73058d8e-48e3-92a2-875f-46ec2e4be572][][scale=0.3]

//indepimage[725e0e51-7be2-9588-5198-f9fa734a28bc][][scale=0.3]




10/5　これで最後の観察、一つだけできたゴーヤの実はいつの間にか熟してオレンジ色になって爆ぜてました。赤色の果肉の付いた種が落ちていて赤い果肉をなめたらめちゃくちゃ甘かったです、ゴーヤは熟すと甘くなる・・・？この果肉の中はザラザラの楕円の種がありました、この種も来年また植えてみよう思います。ツルムラサキはもう半端なく成長し続けているので根本から40cmずつハサミで切りまして捨てようと思います。10月の5日についに緑のカーテン完成といった感じです、もう既に涼しい・・・
//indepimage[ef32b13f-30b9-2374-11e4-755f0dee6a17][][scale=0.3]

//indepimage[654892c5-c1af-114d-6fbd-2b115e37c62a][][scale=0.3]




11/16　やっとキャロライナ・リーパーの実が色づき始めた、秋葉原の11月は結構肌寒いけど株自体も元気のようだ、実も頑張ってちぎらないとちぎれないくらいには強く結実している。1株だけだが20個ほど直径3センチのキャロライナ・リーパーが収穫できた。収穫する手が熱くなるほどの辛さ、半径1mくらいの雰囲気が辛くなって目が痛くなる。すごい。
//indepimage[464b44ef-e46a-042d-ab00-428c240d7524][][scale=0.3]




千代田区のゴーヤ・ツルムラサキのベランダ緑化プロジェクトにも参加しレポートを送りました。
天然素材の麻のショルダーバッグを貰えました良かったですね。
//indepimage[d4eb16e3-7fa6-7ed7-423d-203fc4fa35eb][][scale=0.3]

//indepimage[6ef2db41-6218-3569-687f-39890a896737][][scale=0.3]



== ■スマートホームの延長線上の自動農園についての感想


セットアップをしてからの8ヶ月間全くのメンテナンスフリーでした。夏場は日に2-3回朝昼晩に1分間スマホの画面からスマートプラグのONOFF設定を設定するだけ、秋になったら朝に1回にスマホの画面から設定変えるだけ。たった2回だけスマホのアプリのタイマー設定をいじっただけでキャロライナ・リーパー20個、ゴーヤの種4個、チョウセンアサガオの種100粒（根っこや茎や葉はトリップ用）を収穫できたのは素晴らしいと思いました。他の野菜や果物を育てたかったらプランターと灌漑システムの最後のホースの栓にジョイントを追加してホースを伸ばすだけ。田舎に土地を買って大規模農園をやりたくなるほど成功したと思います。


== ■課題
 * またたびは効果を最大化するには成長過程で虫の害や猫による傷つけが必要なのでもう少しそのあたりを深掘りしたい
 * ゴーヤは1個しか実ができなかったのでもうもう少し実の量が増える研究が必要
 * チョウセンアサガオは種が大量に収穫できたので麻沸散の研究も行いたい
 * キャロライナ・リーパーは上手く出来た
 * Raspberry Piによる湿度温度土中水分量による散水制御を次回は行いたい
 * ３Dプリンタによるベランダ農園最適化の植木鉢の研究も行いたい



//indepimage[c56893dd-8060-7498-c77c-84e2c304f185][][scale=0.8]




= NVIDIA Jetsonを色々いじってみる。


//raw[|latex| \begin{center} ]
@hichon  @<b>{秋葉原ロボット部}
//raw[|latex| \end{center} ]


=== はじめに


NVIDIA Jetsonをさわる機会があったのでセットアップのメモを残しておく


=== スペックなど


@<href>{https://www.avermedia.co.jp/professional/product-detail/AG411B,Box PC AG411B}


=== OSなど


Ubuntu 18.04 LTS


=== VNCのセットアップ


本体にディスプレイ、キーボード、マウスを接続すれば、そのまま認識して使用できるが、リモートで使用したいのでＶＮＣサーバーをインストールする。
最初はTiger VNCをインストールしようとしていたが何故か画面が表示されなかったのでいろいろ調べた結果、@<href>{https://developer.nvidia.com/embedded/learn/tutorials/vnc-setup,公式のドキュメント}があることを発見。vino-serverを使えと言うことらしい。


//emlist[][bash]{
$ mkdir -p ~/.config/autostart
$ cp /usr/share/applications/vino-server.desktop ~/.config/autostart/
$ cd /usr/lib/systemd/user/graphical-session.target.wants
$ sudo ln -s ../vino-server.service .
$ gsettings set org.gnome.Vino prompt-enabled false
$ gsettings set org.gnome.Vino require-encryption false
$ gsettings set org.gnome.Vino authentication-methods "['vnc']"
$ gsettings set org.gnome.Vino vnc-password $(echo -n '(パスワード)'|base64)
$ sudo reboot
//}


手元のパソコンにはUltra VNCをインストールして接続を確認。



簡単ですが今回はここまで。


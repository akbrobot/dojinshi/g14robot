
= FreeCADの作図方法によるファイルサイズの違いの調査～その3


//raw[|latex| \begin{center} ]
たいちゃん  @<b>{秋葉原ロボット部}
//raw[|latex| \end{center} ]


== ご挨拶


こんにちは。たいちゃんです。
およそ1年ぶりの投稿ですが、今年も残り1ヶ月を切ってしまいました。
今回はいつも3Dプリンタ用のデータを作るのに愛用している、FreeCADのファイルサイズが作図方法によって変わるのかの第3段を調査してみたいと思います。
主な調査内容としては、今年は『Part Design』の使い方が少しだけ分かったので、この『Part Design』のファイルサイズを今迄の方法と比較してみたいと思います。


== 前回は・・・
 * 中実図形である図形C、中空図形である図形D共にSketcher+押し出しが最も容量が小さくなりました。
 * 図形CではDraft+押し出しが、図形DではSketcher+押し出しが最も容量が大きくなりました。
 * 以上のことから回転を使用できる図形でも可能であれば押し出しを使用した方が容量を小さく出来る可能性があると考察しました。
 * 図形Dの結果については前回を含め僅差とは言え、Draftを使用した方が容量が小さくなるというのは意外でした。要因が何かは検討がつきませんでした。



前回のリンクはこちらです。



FreeCADの作図方法によるファイルサイズの違いの調査～その2



https://qiita.com/toshita172/items/e0cb67e51046209e5f3a



更に前々回のリンクはこちらです。



FreeCADの作図方法によるファイルサイズの違いの調査～その1



https://qiita.com/toshita172/items/05fdde3cbe7464f3c52a


== それでは調査です。

=== 動作環境
 * m-Book X400-HS　(CPU : Core i7-8565U メモリ : 16GB)
 * Debian GNU/Linux10(buster)
 * FreeCAD 0.18で前回と同様です。


=== 調査A(これは『その1』のものと同じものです。詳しくは上の前々回のリンクをご参照下さい


幅×奥行き×高さ=40mm×50mm×30mmの直方体で調査
→これを『図形A』とおきます。


==== Part Designで・・・


//indepimage[e06ee320-92fa-0fa5-13c7-289c6b1db981][][scale=0.3]

まず、フィーチャーを選択します。
//indepimage[4936729e-a514-8693-7743-c2ddf21fe7b6][][scale=0.4]

次にPart DesignのままSketcherで幅40mm 、高さ50mmの長方形を作ります。(左下の頂点を原点に固定)
//indepimage[ede962fb-c112-fddb-3b64-54e55d949cec][][scale=0.4]

Padのコマンドを使って完成です。


==== FreeCADのファイルであるFCStdファイルは・・・


//indepimage[55b113b9-49de-3137-a076-0eb79e29441e]

Part DesignでのFCStdファイルの容量
//indepimage[a9d70796-83e9-1be4-a269-b2e9f63590a3]

SketcherでのFCStdファイルの容量
//indepimage[b0cd20ba-349f-4cde-b100-7667e1fcdf61]

Partの立方体でのFCStdファイルの容量
//indepimage[70c0da83-42e9-3506-275a-db8fb2b6ec7e]

DraftでのFCStdファイルの容量



『その1』のデータと比較すると、今回のPart DesignはFreeCADの標準形式であるFCStd形式の場合8,049byteと、過去の他の3つよりもズバ抜けて大きい容量となりました。


==== 3Dプリンタ造形用ファイルを作る為のSTLファイルは・・・


//indepimage[92421fc2-7dce-094e-6b7f-679be4a22627]

Part DesignでのSTLファイルの容量
//indepimage[594a99ba-45db-8435-1540-80e142af5826]

SketcherでのSTLファイルの容量
//indepimage[47a9dd03-5535-a5b3-b051-da3a6eccc31f]

Partの立方体でのSTLファイルの容量
//indepimage[4752ed15-af61-9bf3-0318-5dadda537120]

DraftでのSTLファイルの容量



STLファイルについては差は出ませんでした。


=== 調査B(これも『その1』のものと同じものです。詳しくは上の前々回のリンクをご参照下さい。


調査Aの幅×奥行き×高さ=40mm×50mm×30mmの直方体に、上部から幅×奥行き=20mm×30mmの長方形にて壁厚10mmになるように中抜きをした図形で調査
→これを『図形B』とおきます。


==== Part Designで・・・


//indepimage[c72e280d-75d4-aa72-cf1d-4b5ed0ca143b][][scale=0.4]

先程と同様フィーチャーを選択後、次にPart DesignのままSketcherで上図のような図形を作ります。
//indepimage[f22fb93f-4b5b-6208-dc65-5421132483c1][][scale=0.4]

完成です。


==== FreeCADのファイルであるFCStdファイルは・・・


//indepimage[a390b60c-c1f5-e2e7-0e8c-236162af5cc4]

Part DesignでのFCStdファイルの容量
//indepimage[ff62cd0a-5fa2-27b9-274f-09fc003d4b99]

SketcherでのFCStdファイルの容量
//indepimage[35205b88-0471-8bea-7491-dffa91f2c7a6]

Partの立方体でのFCStdファイルの容量
//indepimage[7bc4bd96-e05d-6c23-1161-16693718ecf5]

DraftでのFCStdファイルの容量



『その1』のデータと比較すると、今回のPart DesignはFreeCADの標準形式であるFCStd形式の場合9,448byteとなりました。過去の他の3つの内、最も大きかったDraftの10,014byteよりも若干小さい容量でした。


==== 3Dプリンタ造形用ファイルを作る為のSTLファイルは・・・


//indepimage[04bef5cd-1cef-5443-28fa-d497d18c1502]

Part DesignでのSTLファイルの容量
//indepimage[949f0bec-1dd8-9a2a-250d-245dbf56d408]

SketcherでのSTLファイルの容量
//indepimage[30ccf4c7-577e-f6d7-c412-61ae0ea46dda]

Partの立方体でのSTLファイルの容量
//indepimage[212da3b4-778c-8540-ab0b-eb7fd37e31f7]

DraftでのSTLファイルの容量



STLファイルについては差は出ませんでした。


=== 調査E


@<href>{https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/319826/891e70fb-6489-d865-1f9a-b11d5b2682c9.png,FreeCADTestPartDesigneextra01.png}
上図のように『図形A』の中に幅×奥行き=10mm×8mmの貫通穴を設けます。間隙はX方向に8mmずつ、Y方向に10mmずつです。図形の端から貫通穴迄の間隙も同等です。



//indepimage[02f86f48-8e05-7248-d5d1-5c33f16eaa9f][][scale=0.4]

完成品を真上から見ると上図のようになります。
→これを『図形E』とおきます。


==== Part Designでマルチ変換を使う場合


//indepimage[dd21a740-2e75-4eb1-1e08-4bae5c92ff51][][scale=0.4]

まず、Part Designのワークベンチで図形Aを作ります。
//indepimage[e245b2c0-924d-ed56-44e3-9f2d284773d4][][scale=0.4]

次にPart DesignのままSketcherで図を描き、Pocketコマンドにて上図のような貫通穴ををあけます。
//indepimage[25ff916d-a71d-00d2-7d2b-903164da2772][][scale=0.4]

更に、穴をマルチ変換します。
//indepimage[fde9197a-7322-3166-265b-fb376a0a19d6][][scale=0.4]

完成です。


==== Part Designでマルチ変換の代わりにSketcherで作る場合


//indepimage[1f48df37-9460-0c52-317f-97d48898712a][][scale=0.4]

まず、Part DesignのままSketcherで上図のような図を描きます。
//indepimage[909074c5-b8a3-8060-aa8d-3aa5b29803ea][][scale=0.4]

次にPadコマンドにて押し出して上図のような図を作れば完成です。


==== Sketcher+Partの押し出しで作る場合


//indepimage[720305db-2ce4-0bdc-2109-ecbf2e0020d8][][scale=0.4]

まず、Sketcherで上図のような図を描きます。
//indepimage[0479dd23-9b72-59d8-508a-51143db39246][][scale=0.4]

次にPartのワークベンチの押し出しコマンドにて上図のような図を作れば完成です。


==== Draft+Partの押し出しで作る場合


//indepimage[47369927-3f5d-99da-90d9-c77215e2a147][][scale=0.4]

まず、『その1』のときのようにDraft+Partで上図のように『図形A』を作成します。
//indepimage[ab8e2c4e-3035-b5f8-fffd-913239efd3e7][][scale=0.4]

次に同じ要領で『図形A』内に上図のような直方体を作成します。
//indepimage[929fb90a-5efe-2b67-01d1-d43ee8a0afcc][][scale=0.4]

先程の図から『図形A』を取り去ったのが上図です。
//indepimage[d0bf550f-2a29-66d8-b48c-061b18d853af][][scale=0.4]

次にDraftのワークベンチのArrayコマンドにて上図のように複写します。
//indepimage[9eb30549-0c20-e834-60ff-2d6626057bf6][][scale=0.4]

最後にPartのワークベンチにて、複写した図形を『図形A』から差集合で引いて完成です。


==== FreeCADのファイルであるFCStdファイルは・・・


//indepimage[a6b6287c-38dc-1911-b518-69c208ac21c2]

Part Designでマルチ変換を使った場合のFCStdファイルの容量
//indepimage[d114476b-1ac7-26f9-c28c-26072dc91ff3]

Part Designでマルチ変換の代わりにSketcherで作った場合のFCStdファイルの容量
//indepimage[dd06a971-9dbf-815f-bc87-ba27c9b6e374]

Sketcher+Partの押し出しで作った場合のFCStdファイルの容量
//indepimage[e916fc8b-becb-e465-d9ce-0339ac0f5a34]

Draft+Partの押し出しで作った場合のFCStdファイルの容量



『図形E』ではPart Designでマルチ変換を使ったものが21,453byteと最も大きく、次いでDraft+Partの14,033byteが大きかったです。また、それに僅差でPart Designでマルチ変換を使わなかったものが続き、一番容量が小さかったのはSketcher+Partでした。


==== 3Dプリンタ造形用ファイルを作る為のSTLファイルは・・・


//indepimage[0b7105bb-a819-080f-e749-6d2ac83a379b]

Part Designでマルチ変換を使った場合のSTLファイルの容量
//indepimage[8987a1a2-1766-7214-215f-c60627c7bbc8]

Part Designでマルチ変換の代わりにSketcherで作った場合のSTLファイルの容量
//indepimage[ccacc0e2-f108-bc11-6413-2dfd209ad744]

Sketcher+Partの押し出しで作った場合のSTLファイルの容量
//indepimage[65f6e673-e83f-745c-af33-a4e7f1fa9af7]

Draft+Partの押し出しで作った場合のSTLファイルの容量



STLファイルについては差は出ませんでした。


=== 考察
 * 今回の調査ではFreeCADの標準形式であるFCStd形式で保存する場合、Part Designワークベンチで作成すると大きめのファイルサイズになることが分かりました。
 * Part Designワークベンチは、 1つのワークベンチで複雑な図形を作成できるメリットがあるのかもしれません。しかし、今回の結果では簡単な図形であれば、他のワークベンチを互いに組み合わせた方がFCStd形式の容量を少なく出来る可能性が高いと言えます。
 * STLデータについては今回も差はありませんでした。



最後迄お付き合い下さり、有難うございました。


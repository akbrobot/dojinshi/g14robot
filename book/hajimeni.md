

# はじめに

## この冊子について

秋葉原支部ロボット部の2022年のアドベントカレンダーで発表した内容をまとめています。

## Qiita での公開

 原稿は、Qiita で公開した記事を、ソーシャル組版システム「Qiita2Review 」を使い製版、製本しています。

元記事は、インターネット上で参照できます。


### 総力特集「仮想空間は世界意識とAIの店の戦いの場だ！」

- 「NVIDIA Jetsonを色々いじってみる。」
 - 秋葉原ロボット部 @hichon
 - https://qiita.com/hichon/items/e5289cb8f524380f753f

- 「意識のクラスモデル（仏教ゴエンカ版）」
 - 秋葉原ロボット部 部長 清水 素釘武
 - https://qiita.com/tshimizu8/items/995e6a472284d1ef6531


- 「ロボティクスで考える知能処理分類」
 - ICI技研
 - https://qiita.com/ICI-giken/items/ef5a0833008a551e79bf

- 「メタバースとは、何ぞや？」
 - 秋葉原ロボット部 武装猛牛
 - https://qiita.com/busyoucow/items/668672e8471c1359ee30


- 「コンピューターにはコマンド、人では空振り。人にはプリーズ。」
 - 秋葉原ロボット部 @KOSAMU
 - https://www.youtube.com/watch?v=DzDQ8NU0fu4

### 特集2「ハルマゲドン後の文明の再構築！」


- 「レゴブロックでエレベーターを作ったV2」
 - 秋葉原ロボット部　熊秀創吉（熊吉らぼ）
 - https://qiita.com/KumahideSoukichi/items/e08e1a1f2480e5b41509

- 「72の法則を検算してみた」
 - 電機メーカー勤務 あおいさや
 - https://qiita.com/La_zlo/items/9f30dd591458671c2af4

- 「6年乗った軽バンとキャンカーの鉛バッテリーを再生してみた」
 - ちっちゃいものくらぶ とも
 - https://qiita.com/tomonnn1/items/2702de83bab8a33b3729

- 「秋葉原自動農園（日記）」
 - 秋葉原ロボット部 村田
 - https://qiita.com/karukaru7/items/a4a26892f719ab5a67e8


### 特集3「初心者を引きずり込む Makers の魔の手！」

- 「2022年12月版　最適な中華基板屋をチョイスする。」
 - 秋葉原ロボット部 やましょう
 - https://qiita.com/qa65000/items/8bc6ff9cf14046ff7d81


- 「FreeCADの作図方法によるファイルサイズの違いの調査～その3」
 - 秋葉原ロボット部 たいちゃん
 - https://qiita.com/toshita172/items/2528d7d4836612692ce6


- 「格安USBロジアナを sigrok と PulseView で利用する」
 - 秘密結社オープンフォース　河野悦昌
 - https://qiita.com/nanbuwks/items/9069165bfb66d8836583

- 「初心者のチップ部品のはんだ付け」
 - うえだ・ふぁくとりー かずえだ
 - https://qiita.com/kazueda/items/f445a9250b9020f6db04

- 「タミヤ・ローラースケートロボの改造」
 - ロボ部 斉藤
 - https://qiita.com/wtoy2saito/items/46bb16641a183658ebeb


- 「FT-991用CATコントローラの製作」
 - 秋葉原ロボット部 pokibon
 - https://qiita.com/pokibon/items/63ebfa3937a0bdc1ff1b

- 「ATtiny Dev boardで遊ぶ」
 - 低レベル勉強会 さとう
 - https://qiita.com/sat0ken/items/a382eae9479cd4b5d027


## ライセンスについて
個別に指定したもの以外は Public Domain (CC0)扱いです。Qiitaに公開した記事ともども、ご自由にご使用下さい。



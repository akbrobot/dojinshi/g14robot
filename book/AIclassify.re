
= ロボティクスで考える知能処理分類


//raw[|latex| \begin{center} ]
ＩＣＩ技研（石井正好）  @<b>{ＡＩ研究所所長}
//raw[|latex| \end{center} ]
ネットワークをやられている方ならＯＳＩ参照モデルというのをご存じと思います。最下層に物理層があり、最上層にアプリケーション層があり、全部で７つの階層の処理構造になっているモデルです。最下層の物理層はハードウェアそのものであって、それに近い下層ほどそのハードウェアの仕様に対応した具現的な処理を行います。最上層のアプリケーション層はメールやブラウザなどといったＰＣアプリケーションソフトウェアそのものであり、それに近い上層ほどソフトウェアからの要求に対応した抽象的な処理を行います。



同じようにＡＩなどにおける知的処理についても、処理抽象度に沿った階層的な構造として解釈できないか？と考えてみました。その検討をまとめてみた結果が図の通りです。
//indepimage[c77d2f3f-357f-2249-e2c1-04468f9589f8]

例えば物をどのように持つか、自転車の乗り方、タイピングの仕方など、私ら大人の人間がそれまでの長い年月をかけて「体で覚えたもの」というのは確実にあると思います。これは一般的に「非陳述記憶」と言われているものですが、脳科学的には人間の小脳に記憶されると言われています。



この記憶部分、制御部分をパッケージングして新たなロボットコントローラとして実現できないか？と考えています。できればニューラルネットワークを使ったニューロコンピュータとしての構成で。



もし興味があれば下記のグーグルドライブから特許第６７９２１３２号公報をご参照ください（けっきょく宣伝になってすみません＾＾；）。



//indepimage[f1febc4d-1e74-d17c-f629-46a3eb24c5ce][][scale=0.4]

https://bit.ly/3psEnxE



以上


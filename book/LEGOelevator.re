
= レゴブロックでエレベーターを作ったV2


//raw[|latex| \begin{center} ]
熊秀創吉  @<b>{熊吉らぼ}
//raw[|latex| \end{center} ]


== レゴブロックでエレベーターを作ったV2

==== 秋葉原ロボット部　熊秀創吉（熊吉らぼ）
 * レゴブロックのテクニック等で製作。
 * ４階建てのエレベーターです。
 * 昇降用、扉開閉用の2つのモータで動作します。モーター (SparkFun　ROB-13258)https://www.sengoku.co.jp/mod/sgk_cart/detail.php?code=EEHD-53A4
 * ScratchとNanoBoardAGを使い動作のプログラムで動作。ちっちゃいものくらぶ：NanoBoardAGhttps://tiisai.ddo.jp/?page_id=935



//indepimage[0903843b-dbc2-0437-9981-28625b034a12][][scale=1]



==== 初期型との変更点
 * カゴ側の扉が開いた時、当たらないように本体枠の幅を大きくした。
 * メンテナンス用に4Fと2Fのフロアが取り外し可能とした。
 * モーターはLEGO用ではなく模型用を使用しビニルチューブ接続とした。
 * カゴとフロア側の扉や爪の位置等を変えた。
 * SWや扉開閉検知、カゴ位置検知の接続は全てNanoBoardの抵抗センサー入力とした。
 * 本体枠からカゴまでの配線の吊り下げを輪ゴムの連続結びからロープにした。


=== 構成
 * 概略図



//indepimage[0b57a5e8-d904-fc1e-4069-a8e758d95e5c][][scale=1]



=== 全体配線図


//indepimage[7cc3fac6-af99-b464-a470-30b1377cc9ca][][scale=0.8]




//pagebreak


=== NanoBoardAGと抵抗インターフェイス
 * 抵抗インターフェイスの固定抵抗と多回転半固定抵抗



//indepimage[76080125-f208-5d02-7410-93de06bb8ab2][][scale=0.40]



=== 各フロアーとカゴ内のSW


//indepimage[f35da181-13cb-2022-7f3e-d13fdd5c775c][][scale=0.45]



=== NanoBoardAG と 回路図


//indepimage[21ae40cf-cc73-9257-5165-df666fa1035b][][scale=0.35]




//indepimage[78ac447a-617e-c0ad-bd07-d395f69192af][][scale=1]



=== 扉駆動機構
 * 扉開閉検出回転VRと扉駆動モーター 及びカゴ位置検出用マイクロSW



//indepimage[3a9f8e83-a403-2ea6-aedc-2e72e224da8f][][scale=0.35]


 * カゴ側の扉の両サイドのツノがフロア側の扉の爪の間に入る。



//indepimage[85c67f60-f041-2f79-1e3a-47dd30e4e24a][][scale=0.35]


 * フロア側とカゴ側の間にある扉閉まり防止SWはカゴ側の「扉開」SWに接続。



//indepimage[53af4b00-0768-d228-7818-e4d6943aa043][][scale=0.35]


 * フロア側の扉



//indepimage[e1d007ec-d7cc-5676-daee-ce0a1478bdd9][][scale=0.35]



=== 昇降メカ
 * 輪ゴムはロープをタイヤで圧着させるためのもの。



//indepimage[696c0623-4b8d-9113-bb80-676ff8fe80a5][][scale=0.4]


 * ハンドルはカゴの床と各フロアの床を合わせた時、カウンタウエイト側にある爪がフロア検出SWに当たるように調整する為のもの。



//indepimage[53bbaf62-b244-acfa-a72f-791d0f188965][][scale=0.4]



=== カウンターウェイト
 * ウエイトを単三電池で代用。



//indepimage[1b707c1b-f401-27f3-b6a7-1afdf61b9926][][scale=0.4]


 * カゴ位置検出用マイクロSW用の爪を装備



//indepimage[a2d4bd73-efce-a24a-107c-8e6986abfe93][][scale=0.4]



=== 本体からカゴまでのケーブル処理
 * 上下に滑車使い輪ゴムはケーブルの緊張用に使用。



//indepimage[e657983f-fcd7-49a1-b2c3-2188f34eba18][][scale=0.5]



=== Scratchプログラム


//indepimage[e02488d6-224c-bdb3-888c-4b41f194aa5f][][scale=0.5]


